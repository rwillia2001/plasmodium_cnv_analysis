# ## ROY WILLIAMS 2017 R-function to Process BaseData format into input tables for downstream scripts, starting with the CNV_caller.R
# # R-function CNV_BaseReadData_ProcessArchiveTo_Subsets
# # R-proj default sets working directory (wd) to (see below) for portability all code is relative to this denoted as ./  (dot slash)
# # > getwd()
# # [1] "D:/Plasmodium/plasmodium_cnv_analysis
# 
# # CODE UNDER-CONSTRUCTION: was previously used as a series of console commands, so its currently only roughly organised
# 
# ## FUTURE FIXES
# # Problem arises from corrupted file formats (space delim) and random columns appearing (_filter)
# # Check all file formats by loading all file names into list and finding the bad files that break the 
# # code. Step through with loop 1-50-100-200 etc. The 2x MOD files are  repaired, there might be other baddies. 
# # The cp with grep fails when there is nothing to drop (-c grep blah).
# 
# #This should return an object with the 3 data sets. Or 1 set, depending on the metadata..........
# 
# # readdata.subsets.processed <- function.BaseReadDataProcessArchiveToSubsetTables(){}
# 
# seed <-data.frame(row.names=1:5300) #init data frame
# temp <- list.files(path="D:/Plasmodium/test_parseOut_Final_vcf_tables", pattern="*.txt")
# datatable_path <- "D:/Plasmodium/test_parseOut_Final_vcf_tables/"
# Unique_ID <-paste(datatable_path, temp[1], sep="")
# 
# #D:\Plasmodium\wd_plasmod\Unique_ID_5300_stub.txt
# stub = read.delim(Unique_ID, header = TRUE, sep="")
# seed <- stub[4] #get row names - colnames = gene
# 
# colnames(seed) <- "seed_gene_ID"
# colnames(seed)
# 
# for (i in 1:6){                  #length(temp)) {
#   working_ID <-paste(datatable_path, temp[i], sep="")
#   print (working_ID)
#   temp3 = read.delim(working_ID, header = TRUE, row.names = NULL, sep="")
#   print (temp3[1:5,])
#   colnames(temp3) <- paste(temp[i], colnames(temp3), sep = "_") #concat filename to col headers contained in file; provenance
#   colnames(temp3)[4] <- "gene"  #add back matching name header watch out for brackets ARGH!
#   sub_cols <- temp3[,-c(1,2,3,5)] # COLS 1=unique_ID 2=chr 3=pos 4=gene 5=function. Drops cols 1, 2,3,5,
#   #sub_cols <- temp3[,5, drop=FALSE] # COLS 1= Name 2= Length 3=EffectiveLength 4= TPM 5=NumReads
#   #all(seed$seed_gene_ID == sub_cols$gene)  #col test since so many names seem to mismatch#
#   seed <- merge(seed, sub_cols, by.x="seed_gene_ID", by.y="gene", all=TRUE)
#   ## seed <- cbind(seed,sub_cols)
# }
# 
# 
# # 
# subSeed <- seed 
# 
# subSeed <- seed[, -grep("filter$", colnames(seed))]  #drops the randomly occuring "_filter" cols (also one "filte")
# subSeed <- na.omit(subSeed)   #drops rows with NAs
# dim(subSeed)
# #Added annotations on the side
# write.table (subSeed, file="MissingFinalFiles_Files_01_18_2017.txt", sep="\t", row.names=TRUE, col.names=NA)
# write.table (seed, file="new_data_8_48_inputFiles.txt", sep="\t", row.names=TRUE, col.names=TRUE)
# #======================================================
# # Load the first table of old data to merge with the new
# 
# #fileName <-'Plasmodium_CNVs_AUTO_table_NumReads_noNAs_5288_466_withAnnotation.txt'
# fileName <-'Plasmodium_CNVs_AUTO_table_NumReads_noNAs_5200_534_forNorm_noVars_noxy.txt'
# dat = read.delim(fileName, header = TRUE, sep="\t")
# 
# dim(dat) #[1] 5288  458; next we have [1] 5288  505
# 
# ## new missing data file:  MissingFinalFiles_Files_01_17_2017.txt
# #dat 5288 x 458
# #subSeed 5299x48 ; 5299x57
# #seed 5299x505  ; [1] 5299  561
# 
# seed <- merge(dat, subSeed, by.x="UNIQID", by.y="seed_gene_ID", all=TRUE)
# #write.table (seed, file="Plasmodium_CNVs_AUTO_table_NumReads_noNAs_5288_505_withAnnotation.txt", sep="\t", row.names=TRUE, col.names=TRUE)
#  write.table (seed, file="New_data.txt", sep="\t", row.names=TRUE, col.names=NA)

# core_dd2 <- read.delim("./data/scratch_readcounts/SubSet_CountsDATA_MDTIP_Other_dd2_TRUE_5142.txt")
# core_3D7 <- read.delim("./data/scratch_readcounts/SubSet_CountsDATA_MDTIP3D7_Other3d7_TRUE_5142.txt")
# additional_dd2 <- read.delim("./data/scratch_readcounts/dd2_new_data_merge_to_old.txt")
# additional_3D7 <- read.delim("./data/scratch_readcounts/3D7_new_data_merge_to_old.txt")
# 
# dim(core_3D7)  #5142x174
# dim(core_dd2)  #5142x84
# dim(additional_3D7) #5299x32
# dim(additional_dd2) #5299x13
# 
# combined_dd2 <- merge(core_dd2, additional_dd2, by.x="UNIQID", by.y="seed_gene_ID", all=TRUE)
# combined_3D7 <- merge(core_3D7, additional_3D7, by.x="UNIQID", by.y="seed_gene_ID", all=TRUE)
# 
# dim(combined_3D7) #5299x205     174+30=204
# dim(combined_dd2) #5299x96   83+11=94
# 
# write.table (combined_3D7, file="combined_3D7.txt", sep="\t", row.names=TRUE, col.names=NA)
# write.table (combined_dd2, file="combined_dd2.txt", sep="\t", row.names=TRUE, col.names=NA)
# 
# combined_dd2 <- read.delim("./data/scratch_readcounts/combined_dd2.txt")
# combined_3D7 <- read.delim("./data/scratch_readcounts/combined_3D7.txt")
# 
# 

# 
# ##########################################################################################
# #Table got renamed to send to EW, and needs to be transposed for filemaker integration
# #Transposing table
# #Load current raw data table (reads vertical)
# #
# file_for_transpose <-'Plasmodium_CNVs_AUTO_table_NumReads_noNAs_5200_546_uniqueSamples_forNorm_noVars_noxy_01192017.txt'
# for_transpose_dat <- read.delim(file_for_transpose, header = TRUE, sep="\t")
# 
# transposed_data <- t(for_transpose_dat)
# head(file_for_transpose)
# dim(for_transpose_dat)
# head(transposed_data)
# dim(transposed_data)
# write.table (transposed_data , file="TRANSPOSED_PF_CNVs_AUTO_table_NumReads_noNAs_5200_534_forNorm_noVars_noxy_plusMissing_547.txt", sep="\t", row.names=TRUE, col.names=NA)
# 
# #########################################################################################
# 
# write.table (seed, file="Plasmodium_CNVs_AUTO_table_NumReads_noNAs_5288_forPurva.txt", sep="\t", row.names=TRUE, col.names=TRUE)
# 
# # Moving over the new group designations to the table with the ADDED missing data (BMGF-Wirth-6767-3-2H5, BMGF-Wirth-6767-3-3D6, BMGF-Winzeler-MDTIP4-3C3, BMGF-Winzeler-MDTIP4-3F10)
# #Table with index and groups info
# fileName <-'masterTableMergedSampleProps_NoDuplicates_UPDATES_534.txt'
# dat_tab1 = read.delim(fileName, header = TRUE, sep="\t")
# 
# fileName <-'ForRoyCompleteCNVs_NewDesignations.txt'
# dat_tab2 = read.delim(fileName, header = TRUE, sep="\t")
# 
# dat_tab_merged  <- merge(dat_tab1, dat_tab2, by.x="Name_from_R_dots_555", by.y="exp_name", all=TRUE)
# write.table (dat_tab_merged, file="masterTableMergedSampleProps_NoDuplicates_UPDATES_534_01182017.txt", sep="\t", row.names=TRUE, col.names=NA)
# 
# #########################################################################################
# ## SUBSETTING THE RAW DATA IN THE 555 SAMPLE TABLE. SPLIT INTO 2 or 3 SMALLER TABLES
# ## This table: "Plasmodium_CNVs_AUTO_table_NumReads_noNAs_5288_561_withAnnotation.txt"
# ## REPLACED!  "Plasmodium_CNVs_AUTO_table_NumReads_noNAs_5200_534_forNorm_noVars_noxy.txt"
# ## of all merged RAW data is used to make the index to split into subsets, followed by subset wise Normalization.
# ############################################################################################
# ## 1/19/2017 New index for data table from EAW incuding missing data
# ##  ExpNames_to_data_col_number_index.txt table has 538 (537 experiments) rows from which only 261 are needed for the publication and to match the annotation table
# ## Load the index table and the 261 annotation table and merge to pick up index numbers
# fileName_261AnnotationTable <- "ForRoyCompleteCNV_01192017_noDupes_261.txt"
# fileName_537RawDataTable <- "ExpNames_to_data_col_number_index.txt"
# 
# New_261AnnotationTable = read.delim(fileName_261AnnotationTable , header = TRUE, sep="\t")
# New_537RawDataTable= read.delim(fileName_537RawDataTable, header = TRUE, sep="\t")
# dim(New_261AnnotationTable)
# dim(New_537RawDataTable)
# # merge on columns exp_name [ForRoyCompleteCNV_01192017_noDupes_261.txt] first with UNIQID [ExpNames_to_data_col_number_index.txt]
# New_261AnnotationTable_plusDataIndex <- merge(New_261AnnotationTable, New_537RawDataTable, by.x="exp_name", by.y="UNIQID", all=TRUE)
# write.table (New_261AnnotationTable_plusDataIndex, file="New_261AnnotationTable_plusDataIndex.txt", sep="\t", row.names=TRUE, col.names=NA)
# # Deleted the not matching excess rows
# #  index column = data_table_col_number  in the table 
# # Reloaded new index table [New_261AnnotationTable_plusDataIndex.txt]
# New_261AnnotationTable_plusDataIndex <- read.delim("New_261AnnotationTable_plusDataIndex.txt" , header = TRUE, sep="\t")
# dim(New_261AnnotationTable_plusDataIndex)  #[1] 261  15
# LogicalSampleTable <- New_261AnnotationTable_plusDataIndex #copied to old object ID
# ############################################################################################
# # make index table with the criteria for subset (MDTIP, MDTIPdd2, otherdd2) matched to col names 555 table
# 
# fileName_masterTable <- "DataTable_ExperimentNames555_ForSubsets.txt"
# fileName_masterTable <- "DataTable_ExperimentNames555_ForSubsets.txt"
# fileName_matchingSampleTable <- "Key_propeties_SampleSubsets.txt"
# 
# masterTable = read.delim(fileName_masterTable , header = TRUE, sep="\t")
# matchingSampleTable = read.delim(fileName_matchingSampleTable, header = TRUE, sep="\t")
# 
# masterTableMergedSampleProps <- merge(masterTable, matchingSampleTable, by.x="col_header_Sample_Name_dots", by.y="DashToDots_R", all=TRUE)
# 
# write.table (masterTableMergedSampleProps, file="masterTableMergedSampleProps.txt", sep="\t", row.names=TRUE, col.names=TRUE)
# 
# #reload table with added TRUE/FALSE cOLUMNS
# fileName_LogicalSampleTable <- "masterTableMergedSampleProps_NoDuplicates.txt"  #CHANGED from table with dupes
# 
# LogicalSampleTable = read.delim(fileName_LogicalSampleTable , header = TRUE, sep="\t")
# 
# #subset matrix for (1) MDTIP and (2) in 3d7 set; TRUE && TRUE
# #index_forSubSet <- LogicalSampleTable[with(LogicalSampleTable, MDTIP_TRUE == 1 & (MDTIP3D7_TRUE == 1 | Other3d7_TRUE == 1)), ] #gives 173 from the 261
# index_forSubSet <- LogicalSampleTable[with(LogicalSampleTable, MDTIP_TRUE == 1 & (MDTIPdd2_TRUE == 1 | Otherdd2_TRUE == 1)), ] #gives 83 out of 261
# index_forSubSet <- LogicalSampleTable[with(LogicalSampleTable, MDTIP_TRUE == 1 & X7G8_TRUE == 1), ]                             #gives 5 out of 261
# index_forSubSet$data_table_col_number
# length(index_forSubSet$data_table_col_number)
# # The 3 groups total to 261 (correct!)
# 
# #index_forSubSet <- LogicalSampleTable[with(LogicalSampleTable, MDTIP_FALSE == 1 ), ]
# #index_forSubSet <- na.omit(index_forSubSet) #remove ROWs with na. NOOOOOOOOOOOOOOOOOOOOOOOOOOO!
# 
# 
# ## Use index to pull experiments from the raw data table 555
# fileName_RawData_538 <- "Plasmodium_CNVs_AUTO_table_NumReads_noNAs_5143_538_uniqueSamples_forNorm_noVars_noxy_01192017.txt" #droped 57 rows of mito and ribosomal stuff
# RawData_538 = read.delim(fileName_RawData_538, header = TRUE, row.names=1, sep="\t")
# 
# #SubSet_CountsDATA_MDTIP3D7_Other3d7_TRUE <- RawData_538[,index_forSubSet$data_table_col_number]
# #SubSet_CountsDATA_MDTIP_Other_dd2_TRUE <- RawData_538[,index_forSubSet$data_table_col_number]
# SubSet_CountsDATA_MDTIP_7G8_TRUE <- RawData_538[,index_forSubSet$data_table_col_number]
# #SubSet_CountsDATA_MDTIP_FALSE <- RawData_538[,index_forSubSet$data_table_col_number]
# #dim(SubSet_CountsDATA_MDTIP3D7_Other3d7_TRUE) #[1] 5200  172 +36 (Other3d7) = 208+3 211
# dim(SubSet_CountsDATA_MDTIP3D7_Other3d7_TRUE) #[1] 5200  117
# dim(SubSet_CountsDATA_MDTIP_FALSE) #[1] 5200  117
# 
# colnames(SubSet_CountsDATA_MDTIP_7G8_TRUE) <- index_forSubSet$Publication_name
# SubSet_CountsDATA_MDTIP3D7_Other3d7_TRUE
# 
# write.table (SubSet_CountsDATA_MDTIP_7G8_TRUE, file="SubSet_CountsDATA_MDTIP_7G8_TRUE_5142.txt", sep="\t", row.names=TRUE, col.names=NA)
# 
# write.table (SubSet_CountsDATA_MDTIP_7G8_TRUE, file="SubSet_Counts_MDTIP_7G8_TRUE_5_pub_names.txt", sep="\t", row.names=TRUE, col.names=NA)
# write.table (SubSet_CountsDATA_MDTIP_FALSE, file="SubSet_CountsDATA_MDTIP_FALSE.txt", sep="\t", row.names=TRUE, col.names=NA)
