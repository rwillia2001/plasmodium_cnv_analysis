[Winzeler Lab, University of California San Diego](http://winzeler.ucsd.edu/)

# Plasmodium Bioinformatics: Exploration of the chemogenetic landscape to define the malaria parasite resistome and drug-able genome #

Chemogenetic characterization through in vitro evolution combined with whole genome analysis is a powerful tool to discover novel antimalarial drug targets and identify drug resistance genes. Our comprehensive genome analysis of 262 Plasmodium falciparum parasites treated with 37 diverse compounds reveals how the parasite evolves to evade the action of small molecule growth inhibitors. This detailed data set revealed 159 gene amplifications and 148 nonsynonymous changes in 83 genes which developed during resistance acquisition. In addition to confirming known multidrug resistance mechanisms, we discovered novel multidrug resistance genes. Furthermore, we identified promising new drug target-inhibitor pairs to advance the malaria elimination campaign, including: thymidylate synthase and a benzoquinazolinone, farnesyltransferase and a pyrimidinedione, and a dipeptidylpeptidase and an
arylurea. This deep exploration of the P. falciparum resistome and drug-able genome will guide future drug discovery and structural biology efforts, while also advancing our understanding of resistance mechanisms of the deadliest malaria parasite. 

![cnv_results_summary.PNG](https://bitbucket.org/repo/6x5dzE/images/1812032468-cnv_results_summary.PNG)


### R Best Practices ###

* [An attempt was made to follow the Google R style Guide](https://google.github.io/styleguide/Rguide.xml)
* [Notes on organising an R-proj in RStudio](https://nicercode.github.io/blog/2013-05-17-organising-my-project/


### Summary of method ###
The R programming language was used to construct a data analysis workflow for the determination of copy number variants based on differential DNA sequence coverage across the P. falciparum genome. The pipeline R functions are available as an RStudio project from https://bitbucket.org/rwillia2001/plasmodium_cnv_analysis and are distributed as open source under the terms of the GNU General Public License. Read coverage was normalized for each set by (1) Log transforming the data and centering genes to the mean (normalize genes) and (2) Center sequencing samples to the sample means (normalize samples). Amplified genes were identified if they showed a 2-fold increase above the mean coverage. We deemed a 4-gene unit as the minimal length for a CNV of biological interest since CNVs of interest typically cover genomic windows of at least 2 genes, and have recombination breakpoints in the neighboring regions. To distinguish between normal coverage fluctuation and regions more likely to harbor biologically significant alterations, we filtered for regions with a higher proportion of adjacent genes with a greater than 2-fold increase. As CNV length increases they become highly unlikely to be picked by random chance as shown by permutation of the data and re-running the CNV filtering function (see permutation R "sample" function). Since these low stringency filters captured small mitotic recombination events that arise during long term growth(20), we further required CNVs to be in the core genome and to have greater than 3x coverage relative to the average over the length of the CNV. Each CNV region in a sample was given a t-test p-value significance by comparison between the CNV region read depth vector and the same region vector in the other samples. Levene's test was used to investigate "variances equal" and probability values were calculated using a Benjamini-Hochberg corrected t-test (one-tailed, variances equal) of the average normalized coverage for all genes in the interval for the P. falciparum clone relative to the average normalized coverage for all genes in the interval for all other clones. To establish the false discovery rate, read coverage was randomized as a function of chromosome position and probability values were calculated. Applying similar filters (average read coverage > 3x, p<0.001), we identified only 8 CNVs in the permuted 3D7 set and 1 in the Dd2 permuted set. Six CNVs that failed the quality threshold but were visually identifiable and present in other clones in the selection group were also added for a final set of 159. CNV deletion events were determined by looking 36 for a significantly lowered normalized sequence coverage of < (-0.05), and following the same workflow as described above.

* Configuration: 
The file HeadMaster_cnv_analysis.R in the top level directory is used to direct the sourcing and execution of the R functions described in the /R directory. Required index file information and other variables for each analysis are supplied in the .yaml files in the /yamlFile directory. 
* Dependencies: 
The parsing of the .yaml files is dependent on installing the R library package YAML. The other libraries loaded at the start of the HeadMaster file are optional.
* Deployment instructions: 
Download the entire repository and unzip onto local computer. Open the top level .Rproj file with Rstudio; the entire directory organisation and files will be revealed in the "File" window tab (bottom right pane)

### Who do I talk to? ###
* twitter: @rwillia2001  (preferred for technical questions)
* Contact Roy Williams: rmw002 at ucsd.edu ; rwillia at scripps.edu (replace at with @)